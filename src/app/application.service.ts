import { Injectable, OnInit } from "@angular/core";
import { Observable, Subject } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class ApplicationService implements OnInit {
  private subject = new Subject<any>();
  private loaderSubject = new Subject<any>();
  message: string;
  constructor() {}

  ngOnInit() {}

  loaderIndicator(flag: boolean) {
    this.loaderSubject.next({ isLoading: flag });
  }

  loading() {
    return this.loaderSubject.asObservable();
  }

  sendMessage(message: string) {
    this.loaderIndicator(true);
    setTimeout(() => {
      this.subject.next({ text: message });
      this.loaderIndicator(false);
    }, 1000);
  }

  clearMessage() {
    this.subject.next();
  }

  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }
}
