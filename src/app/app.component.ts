import { Component } from "@angular/core";
import { ApplicationService } from "./application.service";
import { Subscription } from "rxjs";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "";
  subscription: Subscription;
  constructor(private appService: ApplicationService) {
    this.subscription = this.appService
      .getMessage()
      .subscribe(res => (this.title = res.text));
  }

  ngOnChanges() {
    this.appService.clearMessage();
  }

  ngOnDestory() {
    this.subscription.unsubscribe();
  }
}
