import { Component, OnInit } from "@angular/core";
import { ApplicationService } from "../application.service";

@Component({
  selector: "loading",
  template: `
    <div *ngIf="isLoading" class="overlay">
      <div class="loading">
        <i
          class="fa fa-circle-o-notch fa-spin"
          style="font-size:24px; color: blue"
        ></i>
      </div>
    </div>
  `,
  styles: [
    `
      .overlay {
        background-color: rgba(0, 0, 0, 0.23);
        position: fixed;
        z-index: 11000;
        left: 0;
        top: 0;
        bottom: 0;
        width: 100%;
        display: block;
      }

      .loading {
        postision: absolute;
        z-index: 9999;
        padding-top: 25%;
        margin-left: 50%;
      }
    `
  ]
})
export class LoadingComponent implements OnInit {
  isLoading: boolean;
  constructor(private lodingService: ApplicationService) {}

  ngOnInit() {
    this.lodingService.loading().subscribe(res => {
      this.isLoading = res.isLoading;
      console.log(res.isLoading);
    });
  }
}
