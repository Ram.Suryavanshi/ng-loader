import { Component, OnInit } from "@angular/core";
import { Subject, Observable } from "rxjs";
import { ApplicationService } from "../application.service";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  message: string;
  constructor(private appService: ApplicationService) {}

  sendMessage() {
    this.appService.sendMessage(this.message);
    this.message = "";
  }

  ngOnInit() {}
}
